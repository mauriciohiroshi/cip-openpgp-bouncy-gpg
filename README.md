# cip-openpgp-bouncy-gpg

# ============================================================ #
Bateria de testes

	Biblioteca BouncyCastle versao 1.64
	Implementacao utilizada: https://neuhalje.github.io/bouncy-gpg/
	Testes realizados entre ambientes Windows e Linux: 
		geracao de chaves, criptografia, descriptografia, assinatura, validacao de assinatura

	# objetivos (testando entre os dois ambientes)
	1. criptografia e descriptografia (sem assinatura) no BouncyCastle
	2. criptografia com assinatura e descriptografia com validacao de assinatura no BouncyCastle
	3. criptografia com assinatura no GnuPG e descriptografia com validacao de assinatura no BouncyCastle
	4. criptografia com assinatura no BouncyCastle e descriptografia com validacao de assinatura no GnuPG
	5. descriptografia com validacao de assinatura com chave publica, sem keyring (colecao) de chaves no BouncyCastle
	6. invalidar assinatura de uma origem nao conhecida (chave publica nao existente na pubring) no BouncyCastle 
	7. criptografia com assinatura no BouncyCastle para multiplos destinos e descriptografia com validacao de assinatura e cada 
		estino com sua chave privada no BouncyCastle 

	# classe de testes unitarios cumprindo o objetivo acima
	br.org.cip.pgp.bouncygpg.BouncyCastlePgpUtilTest
	
# ============================================================ #
GPG

	gpg --version
	Linux: 2.2.4
	Windows: 2.2.21

# ============================================================ #
JDK

	java -version
	Linux (ambiente CIP): 1.8.0_171-b11 (IBM)
	Windows: 1.8.0_211-b12

# ============================================================ #
Chaves 

	# geracao da chave primaria (algoritmo RSA, 2048 bytes, 6 anos de validade)
	gpg --batch --quick-gen-key "NOME <EMAIL>" RSA2048 cert 6y

	# listar chaves da colecao de chaves do ambiente (exibir KeyId em formato LONG)
	gpg --list-keys --keyid-format LONG
	gpg --list-secret-keys --keyid-format LONG

	# adicionar funcionalidade de criptografia e assinatura na chave primária (com validade de 1 ano)
	gpg --batch --quick-add-key <FINGERPRINT> RSA2048 sign 1y
	gpg --batch --quick-add-key <FINGERPRINT> RSA2048 encrypt 1y

	# exclusao de chaves do ambiente
	gpg --delete-keys
	gpg --delete-secret-keys
	
	# geracao no ambiente Linux (par de chaves)
	Linux:
		CIP-BANCOS <cip@cip.com> 
		Senha: ciprsa2048

		CLARO-TELECOM <claro@claro.com>
		Senha: clarorsa2048

	# geracao no ambiente Windows (par de chaves)
	Windows: 
		Win-RSA2048-Key-6y <win-rsa2048-key-6y@email.com>
		Senha: winrsa2048

		OI-TELECOM <oi@oi.com>
		Senha: oirsa2048

		# chave publica da vivo nao importada no ambiente Linux propositalmente para testes negativos
		VIVO-TELECOM <vivo@vivo.com>
		Senha: vivorsa2048

	# exportacao de chaves (exporta a chave especifica)
	gpg --armor --output <ARQUIVO GERADO> --export <FINGERPRINT/USER ID>
	gpg --armor --output <ARQUIVO GERADO> --export-secret-keys <FINGERPRINT/USER ID>
	gpg --armor --output <ARQUIVO GERADO> --export-secret-subkeys <FINGERPRINT/USER ID>

	# exportacao de colecao de chaves (exporta colecao 'pubring' de chaves)
	gpg --armor --output <ARQUIVO GERADO> --export
	gpg --armor --output <ARQUIVO GERADO> --export-secret-keys

	# importacao de chaves
	gpg --import <ARQUIVO>
	
	# alteracao do nivel de confianca de uma chave
	Linux: (echo 5; echo y) | gpg --command-fd 0 --edit-key <KEY-ID> trust quit
	Windows: $(echo 5; echo y) | gpg --command-fd 0 --edit-key <KEY-ID> trust quit
	
	# escolha de qual chave primaria utilizar num comando (quando o ambiente tem mais de uma chave primaria)
	gpg --local-user <FINGERPRINT> ... comando

# ============================================================ #
Criptografia, Descriptografia e Assinatura no GnuPG

	# criptografar (para multipls destinos '-r <FINGERPRINT/USER ID>')
	gpg --output <ARQUIVO GERADO> -e -r <FINGERPRINT/USER ID> <ARQUIVO ORIGEM>

	# criptografar e assinar (para multipls destinos '-r <FINGERPRINT/USER ID>')
	gpg --output <ARQUIVO GERADO> -se -r <FINGERPRINT/USER ID> <ARQUIVO ORIGEM>

	# descriptografar e validar assinatura
	gpg --output <ARQUIVO GERADO> -d <ARQUIVO CRIPTOGRAFADO>

# ============================================================ #
Ambiente Windows 
	
	# listando chaves privadas
	gpg --list-secret-keys --keyid-format LONG
	
		sec   rsa2048/5A84277EB7A01556 2020-08-23 [C] [expires: 2026-08-22]
			  69553412D96B720F73A259A35A84277EB7A01556
		uid                 [ultimate] Win-RSA2048-Key-6y <win-rsa2048-key-6y@email.com>
		ssb   rsa2048/4B875FE8F403C869 2020-08-23 [S] [expires: 2021-08-23]
		ssb   rsa2048/DC3E2985CA9B8588 2020-08-23 [E] [expires: 2021-08-23]

		sec   rsa2048/4EEE071157C9F7AC 2020-08-30 [C] [expires: 2026-08-29]
			  9C6C467024AED5D9739D97204EEE071157C9F7AC
		uid                 [ultimate] VIVO-TELECOM <vivo@vivo.com>
		ssb   rsa2048/0E613A0633B5F1D4 2020-08-30 [S] [expires: 2021-08-30]
		ssb   rsa2048/4189E4A7C596FED2 2020-08-30 [E] [expires: 2021-08-30]

		sec   rsa2048/8C9980345E1BE1E8 2020-08-30 [C] [expires: 2026-08-29]
			  2218D7D6213D4F67BBD0DEA88C9980345E1BE1E8
		uid                 [ultimate] OI-TELECOM <oi@oi.com>
		ssb   rsa2048/3F3C6ECD3D4A0D6E 2020-08-30 [S] [expires: 2021-08-30]
		ssb   rsa2048/DC2BE6641B4DDD56 2020-08-30 [E] [expires: 2021-08-30]
	
	# listando chaves publicas
	gpg --list-keys --keyid-format LONG
	
		pub   rsa2048/5A84277EB7A01556 2020-08-23 [C] [expires: 2026-08-22]
			  69553412D96B720F73A259A35A84277EB7A01556
		uid                 [ultimate] Win-RSA2048-Key-6y <win-rsa2048-key-6y@email.com>
		sub   rsa2048/4B875FE8F403C869 2020-08-23 [S] [expires: 2021-08-23]
		sub   rsa2048/DC3E2985CA9B8588 2020-08-23 [E] [expires: 2021-08-23]

		pub   rsa2048/39C00ABDFCFA6D87 2020-08-26 [C] [expires: 2026-08-25]
			  FE81750B7AFE42DD3D7ACB5839C00ABDFCFA6D87
		uid                 [  full  ] CIP-BANCOS <cip@cip.com>
		sub   rsa2048/7D31D8ED4D37C482 2020-08-26 [S] [expires: 2021-08-26]
		sub   rsa2048/632A08FC29A2B1B9 2020-08-26 [E] [expires: 2021-08-26]

		pub   rsa2048/4EEE071157C9F7AC 2020-08-30 [C] [expires: 2026-08-29]
			  9C6C467024AED5D9739D97204EEE071157C9F7AC
		uid                 [ultimate] VIVO-TELECOM <vivo@vivo.com>
		sub   rsa2048/0E613A0633B5F1D4 2020-08-30 [S] [expires: 2021-08-30]
		sub   rsa2048/4189E4A7C596FED2 2020-08-30 [E] [expires: 2021-08-30]

		pub   rsa2048/8C9980345E1BE1E8 2020-08-30 [C] [expires: 2026-08-29]
			  2218D7D6213D4F67BBD0DEA88C9980345E1BE1E8
		uid                 [ultimate] OI-TELECOM <oi@oi.com>
		sub   rsa2048/3F3C6ECD3D4A0D6E 2020-08-30 [S] [expires: 2021-08-30]
		sub   rsa2048/DC2BE6641B4DDD56 2020-08-30 [E] [expires: 2021-08-30]

# ============================================================ #
Ambiente Linux

	# listando chaves privadas
	gpg --list-secret-keys --keyid-format LONG

        sec   rsa2048/39C00ABDFCFA6D87 2020-08-26 [C] [expires: 2026-08-25]
              FE81750B7AFE42DD3D7ACB5839C00ABDFCFA6D87
        uid                 [ultimate] CIP-BANCOS <cip@cip.com>
        ssb   rsa2048/7D31D8ED4D37C482 2020-08-26 [S] [expires: 2021-08-26]
        ssb   rsa2048/632A08FC29A2B1B9 2020-08-26 [E] [expires: 2021-08-26]
        
        sec   rsa2048/CA63A5D98ECE4D57 2020-08-28 [C] [expires: 2026-08-27]
              4C79185615392AE7F5EFFD16CA63A5D98ECE4D57
        uid                 [ultimate] CLARO-TELECOM <claro@claro.com>
        ssb   rsa2048/DBB9283081288E03 2020-08-28 [S] [expires: 2021-08-28]
        ssb   rsa2048/BC12CF0069B015BF 2020-08-28 [E] [expires: 2021-08-28]

	# listando chaves publicas
	gpg --list-keys --keyid-format LONG
        
        pub   rsa2048/5A84277EB7A01556 2020-08-23 [C] [expires: 2026-08-22]
              69553412D96B720F73A259A35A84277EB7A01556
        uid                 [  full  ] Win-RSA2048-Key-6y <win-rsa2048-key-6y@email.com>
        sub   rsa2048/4B875FE8F403C869 2020-08-23 [S] [expires: 2021-08-23]
        sub   rsa2048/DC3E2985CA9B8588 2020-08-23 [E] [expires: 2021-08-23]
        
        pub   rsa2048/39C00ABDFCFA6D87 2020-08-26 [C] [expires: 2026-08-25]
              FE81750B7AFE42DD3D7ACB5839C00ABDFCFA6D87
        uid                 [ultimate] CIP-BANCOS <cip@cip.com>
        sub   rsa2048/7D31D8ED4D37C482 2020-08-26 [S] [expires: 2021-08-26]
        sub   rsa2048/632A08FC29A2B1B9 2020-08-26 [E] [expires: 2021-08-26]
        
        pub   rsa2048/CA63A5D98ECE4D57 2020-08-28 [C] [expires: 2026-08-27]
              4C79185615392AE7F5EFFD16CA63A5D98ECE4D57
        uid                 [ultimate] CLARO-TELECOM <claro@claro.com>
        sub   rsa2048/DBB9283081288E03 2020-08-28 [S] [expires: 2021-08-28]
        sub   rsa2048/BC12CF0069B015BF 2020-08-28 [E] [expires: 2021-08-28]
        
        pub   rsa2048/8C9980345E1BE1E8 2020-08-30 [C] [expires: 2026-08-29]
              2218D7D6213D4F67BBD0DEA88C9980345E1BE1E8
        uid                 [ultimate] OI-TELECOM <oi@oi.com>
        sub   rsa2048/3F3C6ECD3D4A0D6E 2020-08-30 [S] [expires: 2021-08-30]
        sub   rsa2048/DC2BE6641B4DDD56 2020-08-30 [E] [expires: 2021-08-30]
