package br.org.cip.pgp.bouncygpg;

import name.neuhalfen.projects.crypto.bouncycastle.openpgp.BouncyGPG;
import name.neuhalfen.projects.crypto.bouncycastle.openpgp.keys.callbacks.KeyringConfigCallbacks;
import name.neuhalfen.projects.crypto.bouncycastle.openpgp.keys.keyrings.KeyringConfig;
import name.neuhalfen.projects.crypto.bouncycastle.openpgp.keys.keyrings.KeyringConfigs;
import org.bouncycastle.util.io.Streams;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Classe para criptografia e assinatura de arquivos via linha de comando. <br>
 * Criada para validacao e automacao de testes. <br>
 *
 * @author Mauricio H Tomosada <mauricio.tomosada@essenceit.com>
 */
public class BouncyCastlePgpEncryptMain {

    public static void main(String[] args) {
        if (args.length != 7) {
            System.err.format("Entre com os argumentos: 'remetente' 'destinatario' 'chavePublica' 'chavePrivada' " +
                    "'senha' 'arquivoOrigem' 'arquivoDestino' \n");
            System.exit(-1);

        } else {
            try {
                final String sender = args[0];
                final String recipient = args[1];
                final File pubKeyRing = new File(args[2]);
                final File secKeyRing = new File(args[3]);
                final String secKeyRingPassword = args[4];
                final Path sourceFile = Paths.get(args[5]);
                final Path destFile = Paths.get(args[6]);

                final BouncyCastlePgpUtil pgpUtil = new BouncyCastlePgpUtil();
                pgpUtil.encryptAndSign(sourceFile, destFile, pubKeyRing, secKeyRing, sender, secKeyRingPassword, recipient);
                System.out.println("Criptografia realizada com sucesso.");

            } catch (Exception e) {
                System.err.format("Ocorreu um erro na execucao da criptografia. \n");
                e.printStackTrace();
                System.exit(-1);
            }
        }
    }
}
