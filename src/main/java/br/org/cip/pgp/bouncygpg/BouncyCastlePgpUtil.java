package br.org.cip.pgp.bouncygpg;

import static name.neuhalfen.projects.crypto.bouncycastle.openpgp.keys.keyrings.KeyringConfigs.withKeyRingsFromFiles;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;

import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.util.io.Streams;

import name.neuhalfen.projects.crypto.bouncycastle.openpgp.BouncyGPG;
import name.neuhalfen.projects.crypto.bouncycastle.openpgp.keys.callbacks.KeyringConfigCallback;
import name.neuhalfen.projects.crypto.bouncycastle.openpgp.keys.callbacks.KeyringConfigCallbacks;
import name.neuhalfen.projects.crypto.bouncycastle.openpgp.keys.keyrings.KeyringConfig;

/**
 * Classe utilitaria para criptografia, descriptografia, assinatura e validacao
 * de assinatura de arquivos no padrao PGP. <br>
 * 
 * @author Mauricio H Tomosada <mauricio.tomosada@essenceit.com>
 */
public class BouncyCastlePgpUtil {

	private static final int BUFFSIZE = 8 * 1024;

	/**
	 * Criptografa arquivo sem assinatura. <br>
	 * 
	 * @param plainFile     arquivo original a ser criptografado
	 * @param encryptedFile arquivo de saida criptografado
	 * @param publicKeyring arquivo de chaves publicas
	 * @param secretKeyring arquivo de chaves privadas
	 * @param recipient     user id do destinatario (e-mail)
	 * 
	 * @throws IOException              em caso de erro de leitura de arquivo
	 * @throws PGPException             em caso de erro durante a criptografia
	 * @throws NoSuchProviderException  em caso de erro durante a criptografia
	 * @throws NoSuchAlgorithmException em caso de erro durante a criptografia
	 * @throws SignatureException       em caso de erro de assinatura
	 */
	public void encrypt(final Path plainFile, final Path encryptedFile, final File publicKeyring,
			final File secretKeyring, final String... recipients)
			throws IOException, SignatureException, NoSuchAlgorithmException, NoSuchProviderException, PGPException {
		this.encryptAndSign(plainFile, encryptedFile, publicKeyring, secretKeyring, null, null, recipients);
	}

	/**
	 * Criptografa arquivo e o assina com chave do remetente. <br>
	 * 
	 * @param plainFile     arquivo original a ser criptografado
	 * @param encryptedFile arquivo de saida criptografado
	 * @param publicKeyring arquivo de chaves publicas
	 * @param secretKeyring arquivo de chaver privadas
	 * @param sender        user id do remetente (e-mail)
	 * @param passphrase    senha da chave privada da assinatura
	 * @param recipient     user id do destinatario (e-mail)
	 * 
	 * @throws IOException              em caso de erro de leitura de arquivo
	 * @throws PGPException             em caso de erro durante a criptografia
	 * @throws NoSuchProviderException  em caso de erro durante a criptografia
	 * @throws NoSuchAlgorithmException em caso de erro durante a criptografia
	 * @throws SignatureException       em caso de erro de assinatura
	 */
	@SuppressWarnings("deprecation")
	public void encryptAndSign(final Path plainFile, final Path encryptedFile, final File publicKeyring,
			final File secretKeyring, final String sender, final String passphrase, final String... recipients)
			throws IOException, SignatureException, NoSuchAlgorithmException, NoSuchProviderException, PGPException {

		BouncyGPG.registerProvider();
		KeyringConfigCallback callback = sender != null ? KeyringConfigCallbacks.withPassword(passphrase)
				: KeyringConfigCallbacks.withUnprotectedKeys();
		final KeyringConfig keyringConfig = withKeyRingsFromFiles(publicKeyring, secretKeyring, callback);

		try (final OutputStream fileOutput = Files.newOutputStream(encryptedFile);
				final BufferedOutputStream bufferedOut = new BufferedOutputStream(fileOutput, BUFFSIZE);

				final OutputStream outputStream = sender != null
						? this.encryptAndSign(keyringConfig, sender, bufferedOut, recipients)
						: this.encryptWithoutSignature(keyringConfig, bufferedOut, recipients);

				final InputStream is = Files.newInputStream(plainFile)) {
			Streams.pipeAll(is, outputStream);
		}
	}

	/**
	 * Descriptografa arquivo sem assinatura do remetente. <br>
	 * 
	 * @param encryptedFile arquivo criptografado e assinado
	 * @param plainFile     arquivo de saida descriptografado
	 * @param publicKeyring arquivo de chaves publicas
	 * @param secretKeyring arquivo de chaves privadas
	 * @param passphrase    senha da chave privada do destinatario
	 * 
	 * @throws PGPException             em caso de erro durante a criptografia
	 * @throws SignatureException       em caso de erro de assinatura
	 * @throws NoSuchAlgorithmException em caso de erro durante a criptografia
	 * @throws NoSuchProviderException  em caso de erro durante a criptografia
	 * @throws IOException              em caso de erro de leitura de arquivo
	 */
	public void decrypt(final Path encryptedFile, final Path plainFile, final File publicKeyring,
			final File secretKeyring, final String passphrase)
			throws PGPException, SignatureException, NoSuchAlgorithmException, NoSuchProviderException, IOException {
		this.decrypt(encryptedFile, plainFile, publicKeyring, secretKeyring, passphrase, false);
	}

	/**
	 * Descriptografa arquivo e valida assinatura do remetente. <br>
	 * 
	 * @param encryptedFile arquivo criptografado e assinado
	 * @param plainFile     arquivo de saida descriptografado
	 * @param publicKeyring arquivo de chaves publicas
	 * @param secretKeyring arquivo de chaves privadas
	 * @param passphrase    senha da chave privada do destinatario
	 * 
	 * @throws PGPException             em caso de erro durante a criptografia
	 * @throws SignatureException       em caso de erro de assinatura
	 * @throws NoSuchAlgorithmException em caso de erro durante a criptografia
	 * @throws NoSuchProviderException  em caso de erro durante a criptografia
	 * @throws IOException              em caso de erro de leitura de arquivo
	 */
	public void decryptAndVerify(final Path encryptedFile, final Path plainFile, final File publicKeyring,
			final File secretKeyring, final String passphrase)
			throws PGPException, SignatureException, NoSuchAlgorithmException, NoSuchProviderException, IOException {
		this.decrypt(encryptedFile, plainFile, publicKeyring, secretKeyring, passphrase, true);
	}

	@SuppressWarnings("deprecation")
	private void decrypt(final Path encryptedFile, final Path plainFile, final File publicKeyring,
			final File secretKeyring, final String passphrase, final boolean signatureValidation)
			throws PGPException, SignatureException, NoSuchAlgorithmException, NoSuchProviderException, IOException {

		BouncyGPG.registerProvider();
		final KeyringConfig keyringConfig = withKeyRingsFromFiles(publicKeyring, secretKeyring,
				KeyringConfigCallbacks.withPassword(passphrase));

		try (final OutputStream fileOutput = Files.newOutputStream(plainFile);
				final BufferedOutputStream bufferedOut = new BufferedOutputStream(fileOutput, BUFFSIZE);

				final InputStream plaintextStream = signatureValidation
						? this.decryptAndVerify(Files.newInputStream(encryptedFile), keyringConfig)
						: this.decryptWithoutSignatureValidation(Files.newInputStream(encryptedFile), keyringConfig)) {
			Streams.pipeAll(plaintextStream, bufferedOut);
		}
	}

	private InputStream decryptAndVerify(final InputStream encryptedStream, final KeyringConfig keyringConfig)
			throws NoSuchProviderException, IOException {
		return BouncyGPG
				.decryptAndVerifyStream()
				.withConfig(keyringConfig)
				.andValidateSomeoneSigned()
				.fromEncryptedInputStream(encryptedStream);
	}

	private InputStream decryptWithoutSignatureValidation(final InputStream encryptedStream, final KeyringConfig keyringConfig)
			throws NoSuchProviderException, IOException {
		return BouncyGPG
				.decryptAndVerifyStream()
				.withConfig(keyringConfig)
				.andIgnoreSignatures()
				.fromEncryptedInputStream(encryptedStream);
	}

	private OutputStream encryptAndSign(final KeyringConfig keyringSenderConfig, final String sender,
			final BufferedOutputStream bufferedOut, final String... recipients)
			throws PGPException, SignatureException, NoSuchAlgorithmException, NoSuchProviderException, IOException {
		return BouncyGPG
				.encryptToStream()
				.withConfig(keyringSenderConfig)
				.withStrongAlgorithms()
				.toRecipients(recipients)
				.andSignWith(sender)
				.binaryOutput()
				.andWriteTo(bufferedOut);
	}

	private OutputStream encryptWithoutSignature(final KeyringConfig keyringSenderConfig,
			final BufferedOutputStream bufferedOut, final String... recipients)
			throws PGPException, SignatureException, NoSuchAlgorithmException, NoSuchProviderException, IOException {
		return BouncyGPG
				.encryptToStream()
				.withConfig(keyringSenderConfig)
				.withStrongAlgorithms()
				.toRecipients(recipients)
				.andDoNotSign()
				.binaryOutput()
				.andWriteTo(bufferedOut);
	}
}
