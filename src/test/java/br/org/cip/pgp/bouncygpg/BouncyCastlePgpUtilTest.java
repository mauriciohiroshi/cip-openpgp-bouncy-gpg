package br.org.cip.pgp.bouncygpg;

import static org.custommonkey.xmlunit.XMLAssert.assertXMLEqual;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.codehaus.plexus.archiver.tar.TarGZipUnArchiver;
import org.codehaus.plexus.logging.console.ConsoleLoggerManager;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runners.MethodSorters;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * Classe de testes unitarios de {@link BouncyCastlePgpUtil}
 * 
 * @author Mauricio H Tomosada <mauricio.tomosada@essenceit.com>
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BouncyCastlePgpUtilTest {

	/**
	 * Keyring de chaves publicas e privadas geradas em ambiente diferentes (Windows e Linux). <br>
	 * Ambiente Windows: JDK 1.8.0_211, GPG v2.2.21 <br>
	 * Ambiente Linux: JDK (IBM) 1.8.0_171, GPG v2.2.4 <br>
	 */
	private static final File WINDOWS_PUB_KEYRING = new File("./src/test/resources/keys/public/win-pubring.gpg");
	private static final File WINDOWS_SEC_KEYRING = new File("./src/test/resources/keys/private/win-secring.gpg");
	private static final File LINUX_PUB_KEYRING = new File("./src/test/resources/keys/public/lin-pubring.gpg");
	private static final File LINUX_SEC_KEYRING = new File("./src/test/resources/keys/private/lin-secring.gpg");

	/**
	 * Chaves individuais para testes especificos de descriptografia apenas. <br>
	 */
	private static final File CIP_PUBLIC_KEY = new File("./src/test/resources/keys/public/CIP-BANCOS-RSA2048-PUB-KEY");
	private static final File CLARO_PRIVATE_KEY = new File("./src/test/resources/keys/private/CLARO-TELECOM-RSA2048-PRIV-KEY");
	private static final File OI_PRIVATE_KEY = new File("./src/test/resources/keys/private/OI-TELECOM-RSA2048-PRIV-KEY");
	private static final File WIN_TELCO_PRIVATE_KEY = new File("./src/test/resources/keys/private/WIN-RSA2048-PRIV-KEY");

	/**
	 * Diretorios de arquivos para testes.
	 */
	private static final String FILE_PATH = "./src/test/resources/files/";
	private static final String ORIGIN_FILE = FILE_PATH + "origin/";
	private static final String ENCRYPTED_FILE = FILE_PATH + "encrypted/";
	private static final String DECRYPTED_FILE = FILE_PATH + "decrypted/";

	/**
	 * Classe utilitaria com as funcionalidades para encriptacao, decriptacao,
	 * assinatura e validacao de assinatura.
	 */
	private static final BouncyCastlePgpUtil bouncyUtil = new BouncyCastlePgpUtil();

    /**
     * Configuracao para excecoes esperadas em testes negativos.
     */
    @Rule
    public ExpectedException thrown = ExpectedException.none();

	/**
	 * Arquivo XML criptografado pela TELCO sem assinatura com chave publica da CIP. <br>
	 * Deve ser descriptografado pela CIP com sua chave privada. <br>
	 * Chave TELCO gerada no Windows. <br>
	 * Chave CIP gerada no Linux. <br>
	 * 
	 * @throws Exception em caso de erro
	 */
	@Test
	public void scenario01TelcoToCipJustEncrypt() throws Exception {
		final Path plainFile = Paths.get(ORIGIN_FILE + "plain.xml");
		final Path encryptedFile = Paths.get(ENCRYPTED_FILE + "from-telco-to-cip.enc");
		bouncyUtil.encrypt(plainFile, encryptedFile, WINDOWS_PUB_KEYRING, WINDOWS_SEC_KEYRING, "cip@cip.com");

		final Path decryptedFile = Paths.get(DECRYPTED_FILE + "from-telco-to-cip.dec.xml");
		bouncyUtil.decrypt(encryptedFile, decryptedFile, LINUX_PUB_KEYRING, LINUX_SEC_KEYRING, "ciprsa2048");

		assertNotNull(plainFile.toFile());
		assertNotNull(decryptedFile.toFile());
		this.xmlDocumentCompare(plainFile.toFile(), decryptedFile.toFile());
	}

	/**
	 * Arquivo XML criptografado e assinado pela TELCO com chave publica da CIP. <br>
	 * Deve ser descriptografado e validada a assinatura pela CIP com sua chave privada. <br>
	 * Chave TELCO gerada no Windows. <br>
	 * Chave CIP gerada no Linux. <br>
	 * 
	 * @throws Exception em caso de erro
	 */
	@Test
	public void scenario02TelcoToCipEncryptAndSign() throws Exception {
		final Path plainFile = Paths.get(ORIGIN_FILE + "plain.xml");
		final Path encryptedFile = Paths.get(ENCRYPTED_FILE + "from-telco-to-cip.enc.sign");
		bouncyUtil.encryptAndSign(plainFile, encryptedFile, WINDOWS_PUB_KEYRING, WINDOWS_SEC_KEYRING,
				"win-rsa2048-key-6y@email.com", "winrsa2048", "cip@cip.com");

		final Path decryptedFile = Paths.get(DECRYPTED_FILE + "from-telco-to-cip.dec.verified.xml");
		bouncyUtil.decryptAndVerify(encryptedFile, decryptedFile, LINUX_PUB_KEYRING, LINUX_SEC_KEYRING, "ciprsa2048");

		assertNotNull(plainFile.toFile());
		assertNotNull(decryptedFile.toFile());
		this.xmlDocumentCompare(plainFile.toFile(), decryptedFile.toFile());
	}

	/**
	 * Descriptografar e verificar assinatura de um arquivo gerado no GPG. <br>
	 * Arquivo 1 gerado no GPG v.2.2.4 no ambiente Linux e aberto com chave Windows. <br>
	 * Arquivo 1 gerado no GPG v.2.2.21 no ambiente Windows e aberto com chave Linux. <br>
	 * Descriptografia BouncyCastle no Java. <br>
	 * 
	 * @throws Exception em caso de erro
	 */
	@Test
	public void scenario03DecryptAndVerifyFileGeneratedInGPG() throws Exception {
		final Path plainFile = Paths.get(ORIGIN_FILE + "plain.xml");

		final Path linuxEncryptedFile = Paths.get(ORIGIN_FILE + "linux-file-gpg-v2.2.4.enc.sign");
		final Path linuxDecryptedFile = Paths.get(DECRYPTED_FILE + "linux-file-gpg-v2.2.4.dec.verified.xml");
		bouncyUtil.decryptAndVerify(linuxEncryptedFile, linuxDecryptedFile, WINDOWS_PUB_KEYRING, WINDOWS_SEC_KEYRING, "winrsa2048");

		assertNotNull(plainFile.toFile());
		assertNotNull(linuxDecryptedFile.toFile());
		this.xmlDocumentCompare(plainFile.toFile(), linuxDecryptedFile.toFile());

		final Path windowsEncryptedFile = Paths.get(ORIGIN_FILE + "windows-file-gpg-v2.2.21.enc.sign");
		final Path windowsDecryptedFile = Paths.get(DECRYPTED_FILE + "windows-file-gpg-v2.2.21.dec.verified.xml");
		bouncyUtil.decryptAndVerify(windowsEncryptedFile, windowsDecryptedFile, LINUX_PUB_KEYRING, LINUX_SEC_KEYRING, "ciprsa2048");

		assertNotNull(plainFile.toFile());
		assertNotNull(windowsDecryptedFile.toFile());
		this.xmlDocumentCompare(plainFile.toFile(), windowsDecryptedFile.toFile());
	}

	/**
	 * Criptografar/assinar arquivo XML no Java e abri-lo no GnuPG. <br>
	 * Esse teste deve ser finalizado abrindo o arquivo no GnuPG Linux e Windows. <br>
	 * 
	 * @throws Exception em caso de erro
	 */
	@Test
	public void scenario04GenerateFileToGPG() throws Exception {
		final Path linuxPlainFile = Paths.get(ORIGIN_FILE + "plain.xml");
		final Path linuxEncryptedFile = Paths.get(ENCRYPTED_FILE + "from-cip-to-telco-gpg-test.enc.sign");
		bouncyUtil.encryptAndSign(linuxPlainFile, linuxEncryptedFile, LINUX_PUB_KEYRING, LINUX_SEC_KEYRING, "cip@cip.com",
				"ciprsa2048", "win-rsa2048-key-6y@email.com");
		assertNotNull(linuxEncryptedFile.toFile());

		final Path windowsPlainFile = Paths.get(ORIGIN_FILE + "plain.xml");
		final Path windowsEncryptedFile = Paths.get(ENCRYPTED_FILE + "from-telco-to-cip-gpg-test.enc.sign");
		bouncyUtil.encryptAndSign(windowsPlainFile, windowsEncryptedFile, WINDOWS_PUB_KEYRING, WINDOWS_SEC_KEYRING,
				"win-rsa2048-key-6y@email.com", "winrsa2048", "cip@cip.com");
		assertNotNull(windowsEncryptedFile.toFile());
	}

	/**
	 * Descriptografar e verificar assinatura com a chave publica da Telco sem utilizacao das keyrings de chaves. <br>
	 * A nao utilizacao das keyrings (secring e pubring) nessa implementacao eh possivel para: <br>
	 * 1. descriptografar arquivo e verificar assinatura. <br>
	 * 2. criptografar arquivo. <br>
	 * Nao eh possivel assinar um arquivo somente com a chave primaria do remetente. <br>
	 * 
	 * @throws Exception em caso de erro
	 */
	@Test
	public void scenario05DecryptAndVerifyWithTelcoPublicKey() throws Exception {
		final Path plainFile = Paths.get(ORIGIN_FILE + "plain.xml");
		final Path encryptedFile = Paths.get(ENCRYPTED_FILE + "from-cip-to-claro.enc.sign");
		bouncyUtil.encryptAndSign(plainFile, encryptedFile, LINUX_PUB_KEYRING, LINUX_SEC_KEYRING, "cip@cip.com",
				"ciprsa2048", "claro@claro.com");
		assertNotNull(encryptedFile.toFile());

		final Path decryptedFile = Paths.get(DECRYPTED_FILE + "from-cip-to-claro.dec.verified.xml");
		bouncyUtil.decryptAndVerify(encryptedFile, decryptedFile, CIP_PUBLIC_KEY, CLARO_PRIVATE_KEY, "clarorsa2048");

		assertNotNull(plainFile.toFile());
		assertNotNull(decryptedFile.toFile());
		this.xmlDocumentCompare(plainFile.toFile(), decryptedFile.toFile());
	}

	/**
	 * Descriptografar e verificar assinatura de um arquivo XML compactado no padrao 'tar.gz'. <br>
	 * Verificar integridade do XML apos descriptografia e descompactacao do arquivo. <br>
	 * 
	 * @throws Exception em caso de erro
	 */
	@Test
	public void scenario06DecryptAndVerifyCompressedFile() throws Exception {
		final Path encryptedFile = Paths.get(ORIGIN_FILE + "from-cip-to-telco.tar.gz.enc.sign");
		final Path decryptedCompressedFile = Paths.get(DECRYPTED_FILE + "from-cip-to-telco.tar.gz.dec.verified");
		bouncyUtil.decryptAndVerify(encryptedFile, decryptedCompressedFile, WINDOWS_PUB_KEYRING, WINDOWS_SEC_KEYRING, "winrsa2048");
		assertNotNull(decryptedCompressedFile.toFile());

		this.uncompress(new File(DECRYPTED_FILE), decryptedCompressedFile);

		final Path plainFile = Paths.get(ORIGIN_FILE + "plain.xml");
		final Path decryptedFile = Paths.get(DECRYPTED_FILE + "plain.xml");
		assertNotNull(plainFile.toFile());
		assertNotNull(decryptedFile.toFile());
		this.xmlDocumentCompare(plainFile.toFile(), decryptedFile.toFile());
	}

	/**
	 * Cenario de recepcao de arquivo assinado por Telco cuja sua chave publica nao existe na pubring da CIP. <br>
	 * Deve gerar excecao de decriptografia. <br>
	 * 
	 * @throws Exception em caso de erro
	 */
	@Test
	public void scenario07InvalidationInVerifySignature() throws Exception {
		final Path plainFile = Paths.get(ORIGIN_FILE + "plain.xml");
		final Path encryptedFile = Paths.get(ENCRYPTED_FILE + "from-unknown-telco-to-cip.enc.sign");
		bouncyUtil.encryptAndSign(plainFile, encryptedFile, WINDOWS_PUB_KEYRING, WINDOWS_SEC_KEYRING, "vivo@vivo.com",
				"vivorsa2048", "cip@cip.com");

		thrown.expect(IOException.class);
		thrown.expectMessage("Failure decrypting");

		final Path decryptedFile = Paths.get(DECRYPTED_FILE + "from-unknown-telco-to-cip.dec.verified.xml");
		bouncyUtil.decryptAndVerify(encryptedFile, decryptedFile, LINUX_PUB_KEYRING, LINUX_SEC_KEYRING, "ciprsa2048");
	}

	/**
	 * Criptografar e assinar XML para mais de um destino. <br>
	 * Qualquer Telco envolvida deve conseguir abrir com sua chave e verificar assinatura da CIP. <br>
	 * 
	 * @throws Exception em caso de erro
	 */
	@Test
	public void scenario08CipToMultipleTelcos() throws Exception {
		final Path plainFile = Paths.get(ORIGIN_FILE + "plain.xml");
		final Path encryptedFile = Paths.get(ENCRYPTED_FILE + "from-cip-to-multi-telcos.enc.sign");
		bouncyUtil.encryptAndSign(plainFile, encryptedFile, LINUX_PUB_KEYRING, LINUX_SEC_KEYRING,
				"cip@cip.com", "ciprsa2048", "win-rsa2048-key-6y@email.com", "oi@oi.com");

		final Path decryptedFileTelco1 = Paths.get(DECRYPTED_FILE + "from-cip-to-multi-telcos-first.dec.verified.xml");
		bouncyUtil.decryptAndVerify(encryptedFile, decryptedFileTelco1, CIP_PUBLIC_KEY, WIN_TELCO_PRIVATE_KEY, "winrsa2048");

		assertNotNull(plainFile.toFile());
		assertNotNull(decryptedFileTelco1.toFile());
		this.xmlDocumentCompare(plainFile.toFile(), decryptedFileTelco1.toFile());		

		final Path decryptedFileTelco2 = Paths.get(DECRYPTED_FILE + "from-cip-to-multi-telcos-second.dec.verified.xml");
		bouncyUtil.decryptAndVerify(encryptedFile, decryptedFileTelco2, CIP_PUBLIC_KEY, OI_PRIVATE_KEY, "oirsa2048");

		assertNotNull(plainFile.toFile());
		assertNotNull(decryptedFileTelco2.toFile());
		this.xmlDocumentCompare(plainFile.toFile(), decryptedFileTelco2.toFile());		
	}

	/**
	 * Apaga arquivos gerados durante os testes.
	 */
	@BeforeClass
	public static void deleteGeneratedFiles() {
		File encryptedFileDir = new File(ENCRYPTED_FILE);
		if (encryptedFileDir.isDirectory()) {
			File[] files = encryptedFileDir.listFiles();
			for (int i = 0; i < files.length; i++) {
				files[i].delete();
			}
		}
		File decryptedFileDir = new File(DECRYPTED_FILE);
		if (decryptedFileDir.isDirectory()) {
			File[] files = decryptedFileDir.listFiles();
			for (int i = 0; i < files.length; i++) {
				files[i].delete();
			}
		}
	}

	/**
	 * Compara o conteudo e estrutura de dois arquivos XML. <br>
	 * 
	 * @param first primeiro arquivo XML
	 * @param second segundo arquivo XML
	 * @throws IOException em caso de erro de leitura dos arquivos
	 * @throws ParserConfigurationException em caso de erro de transformacao
	 * @throws SAXException em caso de erro de transformacao
	 */
	private void xmlDocumentCompare(final File first, final File second)
			throws IOException, ParserConfigurationException, SAXException {
		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		XMLUnit.setIgnoreWhitespace(true);
		Document plainXML = builder.parse(first);
		Document decryptedXML = builder.parse(second);
		assertXMLEqual(plainXML, decryptedXML);
	}

	/**
	 * Descompacta um arquivo de extensao tar.gz
	 * 
	 * @param directory diretorio destino
	 * @param tarGzFile arquivo compactado
	 */
	private void uncompress(final File directory, final Path tarGzFile) {
		final TarGZipUnArchiver tarGzUnArquiver = new TarGZipUnArchiver();
		ConsoleLoggerManager manager = new ConsoleLoggerManager();
		manager.initialize();
		tarGzUnArquiver.enableLogging(manager.getLoggerForComponent(this.getClass().getName()));
		tarGzUnArquiver.setSourceFile(tarGzFile.toFile());
		tarGzUnArquiver.setDestDirectory(directory);
		tarGzUnArquiver.extract();
	}
}
